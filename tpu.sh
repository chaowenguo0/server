pythone -m pip install -U torch torch_xla[tpu] -f https://storage.googleapis.com/libtpu-releases/index.html -f https://storage.googleapis.com/libtpu-wheels/index.html
curl -O https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
curl https://deb.nodesource.com/setup_current.x | bash -
apt install -y --no-install-recommends nodejs ./google-chrome-stable_current_amd64.deb xvfb podman uidmap slirp4netns fuse-overlayfs catatonit x2goserver-xsession
rm -rf google-chrome-stable_current_amd64.deb
cat <<EOF | tee /usr/lib/systemd/system/alexamaster.service
[Service]
Restart=always
ExecStartPre=/usr/bin/npm install --prefix %h patchright tough-cookie @chaowenguo/tlsclient
ExecStartPre=/usr/bin/curl --output-dir %h -o alexamaster.mjs https://dev.azure.com/chaowenguo/ci/_apis/git/repositories/ci/items?path=alexamaster.mjs
ExecStart=/usr/bin/node %h/alexamaster.mjs

[Install]
WantedBy=multi-user.target
EOF
systemctl enable alexamaster --now