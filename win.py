import aiohttp, asyncio, argparse, asyncssh, pathlib, builtins

parser = argparse.ArgumentParser()
parser.add_argument('password')
args = parser.parse_args()
key = asyncssh.import_private_key(pathlib.Path.home().joinpath('.ssh', 'id_ed25519').read_bytes())

subscription = 'bc64100e-dcaf-4e19-b6ca-46872d453f08'

async def main():
    async with aiohttp.ClientSession() as session:
        async with session.post(f'https://login.microsoftonline.com/476b320f-8c9c-46cb-8856-624a5d3c2c2c/oauth2/token', data={'grant_type':'client_credentials', 'client_id':'5ae868e9-2085-43a3-a3ae-9ff32a1b6b50', 'client_secret':'Sqh8Q~hIO_qqDHHoXXFJQJ1t4w7BPpW9B3hUNbbu', 'resource':'https://management.azure.com/'}) as response:
            token = (await response.json()).get('access_token')
        async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/machine/providers/Microsoft.Compute/virtualMachines/win', params={'api-version':'2022-11-01'}, headers={'authorization':f'Bearer {token}'}, json={'location':'eastus', 'properties':{'hardwareProfile':{'vmSize':'Standard_B1s'}, 'osProfile':{'adminUsername':'ubuntu', 'computerName':'win', 'adminPassword':args.password}, 'storageProfile':{'imageReference':{'sku':'2022-datacenter-azure-edition-core-smalldisk', 'publisher':'MicrosoftWindowsServer', 'version':'latest', 'offer':'WindowsServer'}, 'osDisk':{'diskSizeGB':64, 'createOption':'FromImage'}}, 'networkProfile':{'networkInterfaces':[{'id':f'/subscriptions/{subscription}/resourceGroups/machine/providers/Microsoft.Network/networkInterfaces/win'}]}}}) as machine: 
        #async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/machine/providers/Microsoft.Compute/virtualMachines/win?api-version=2021-07-01', headers={'authorization':f'Bearer {token}'}, json={'location':'westus', 'properties':{'hardwareProfile':{'vmSize':'Standard_B1s'}, 'osProfile':{'adminUsername':'ubuntu', 'computerName':'win', 'adminPassword':args.password}, 'storageProfile':{'imageReference':{'sku':'win11-21h2-entn', 'publisher':'MicrosoftWindowsDesktop', 'version':'latest', 'offer':'windows-11'}, 'osDisk':{'diskSizeGB':64, 'createOption':'FromImage'}}, 'networkProfile':{'networkInterfaces':[{'id':'/subscriptions/9046396e-e215-4cc5-9eb7-e25370140233/resourceGroups/machine/providers/Microsoft.Network/networkInterfaces/win'}]}}}) as machine: #不能安装桌面版, 
            if machine.status == 201:
                while True:
                    await asyncio.sleep(builtins.int(machine.headers.get('retry-after')))
                    async with session.get(machine.headers.get('azure-asyncOperation'), headers={'authorization':f'Bearer {token}'}) as _:
                        if (await _.json()).get('status') == 'Succeeded': break                       
        async with session.post(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/machine/providers/Microsoft.Compute/virtualMachines/win/runCommand', params={'api-version':'2022-11-01'}, headers={'authorization':f'Bearer {token}'}, json={'commandId':'RunPowerShellScript', 'script':['Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0', 'Start-Service sshd', f'Set-Content -Force -Path C:/ProgramData/ssh/administrators_authorized_keys -Value "{key.export_public_key().decode().replace(builtins.chr(10), "")}"', 'icacls C:/ProgramData/ssh/administrators_authorized_keys /inheritance:r /grant Administrators:`(F`) /grant SYSTEM:`(F`)', '(cat C:/ProgramData/ssh/sshd_config).replace("#PasswordAuthentication yes", "PasswordAuthentication no") | Set-Content -Path C:/ProgramData/ssh/sshd_config', 'Restart-Service sshd', 'Set-Service -Name sshd -StartupType Automatic', 'New-ItemProperty -Path HKLM:/SOFTWARE/OpenSSH -Name DefaultShell -Value C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe -PropertyType String -Force', 'Add-WindowsCapability -Online -Name ServerCore.AppCompatibility~~~~0.0.1.0', 'mkdir c:/Certbot/live']}) as command:
            if command.status == 202:
                while True:
                    await asyncio.sleep(10)
                    async with session.get(command.headers.get('azure-asyncOperation'), headers={'authorization':f'Bearer {token}'}) as _:
                       if (await _.json()).get('status') == 'Succeeded': break

asyncio.run(main())
# delete vm and os disk