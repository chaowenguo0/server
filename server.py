import oci, asyncssh, aiohttp, asyncio, base64, pathlib, json, builtins, argparse, aredis

parser = argparse.ArgumentParser()
parser.add_argument('--redis', required=True)
parser.add_argument('--password', required=True)
configure = {'user':'ocid1.user.oc1..aaaaaaaalwudh6ys7562qtyfhxl4oji25zn6aapndqfuy2jfroyyielpu3pa', 'key_file':'oci.key', 'fingerprint':'bd:01:98:0d:5d:4a:6f:b2:49:b4:7f:df:43:00:32:39', 'tenancy':'ocid1.tenancy.oc1..aaaaaaaa4h5yoefhbxm4ybqy6gxl6y5cgxmdijira7ywuge3q4cbdaqnyawq', 'region':'us-sanjose-1'}
computeClient = oci.core.ComputeClient(configure)
computeClientCompositeOperations = oci.core.ComputeClientCompositeOperations(computeClient)
for _ in computeClient.list_instances(compartment_id=configure.get('tenancy')).data:
    if _.shape == 'VM.Standard.E2.1.Micro': computeClientCompositeOperations.terminate_instance_and_wait_for_state(_.id, wait_for_states=[oci.core.models.Instance.LIFECYCLE_STATE_TERMINATED])
virtualNetworkClient = oci.core.VirtualNetworkClient(configure)
vcn = virtualNetworkClient.list_vcns(compartment_id=configure.get('tenancy')).data[0]
subnet = virtualNetworkClient.list_subnets(compartment_id=configure.get('tenancy')).data[0]
#virtualNetworkClient.list_security_lists(compartment_id=configure.get('tenancy')).data[0].ingress_security_rules[0].tcp_options.destination_port_range.max = 443
key = asyncssh.import_private_key(pathlib.Path.home().joinpath('.ssh', 'id_ed25519').read_bytes())
publicKey = key.export_public_key().decode()

init = '''sudo apt update
sudo apt purge -y snapd
curl -O https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt install -y --no-install-recommends podman uidmap slirp4netns fuse-overlayfs catatonit ./google-chrome-stable_current_amd64.deb libx11-xcb1 x2goserver-xsession fonts-noto-cjk
rm google-chrome-stable_current_amd64.deb
sudo apt clean'''

async def oracle():
    launchInstanceDetails = oci.core.models.LaunchInstanceDetails(availability_domain=oci.identity.IdentityClient(configure).list_availability_domains(compartment_id=vcn.compartment_id).data[0].name, compartment_id=vcn.compartment_id, shape='VM.Standard.E2.1.Micro', metadata={'ssh_authorized_keys':publicKey}, image_id=computeClient.list_images(compartment_id=vcn.compartment_id, operating_system='Canonical Ubuntu', operating_system_version='22.04').data[0].id, subnet_id=subnet.id)
    instance = computeClientCompositeOperations.launch_instance_and_wait_for_state(launchInstanceDetails, wait_for_states=[oci.core.models.Instance.LIFECYCLE_STATE_RUNNING]).data
    ip = oci.core.VirtualNetworkClient(configure).get_vnic(computeClient.list_vnic_attachments(compartment_id=vcn.compartment_id, instance_id=instance.id).data[0].vnic_id).data.public_ip
    await asyncio.sleep(60)
    async with asyncssh.connect(ip, username='ubuntu', known_hosts=None) as ssh: await ssh.run('sudo apt purge -y snapd\n' + init)
                                # client_keys=[]
    return ip

import google.auth, google.auth.transport.requests, google.oauth2
credentials = google.oauth2.service_account.Credentials.from_service_account_info(json.loads(pathlib.Path(__file__).parent.joinpath('gcloud').read_text()), scopes=['https://www.googleapis.com/auth/cloud-platform'])
client = google.auth.transport.requests.AuthorizedSession(credentials)
project = 'argon-potential-377608'
region = 'us-central1'
zone = region + '-a'

async def gcloud():
    #client.post(f'https://compute.googleapis.com/compute/v1/projects/{project}/global/networks', json={'autoCreateSubnetworks':False, 'name':'network45', 'enableUlaInternalIpv6':True})
    #await asyncio.sleep(30)
    #client.post(f'https://compute.googleapis.com/compute/v1/projects/{project}/regions/{region}/subnetworks', json={'ipCidrRange':'10.0.0.0/24', 'network':f'https://www.googleapis.com/compute/v1/projects/{project}/global/networks/network45', 'name':'network45', 'stackType':'IPV4_IPV6', 'ipv6AccessType':'EXTERNAL'})
    print(client.post('https://compute.googleapis.com/compute/v1/projects/{project}/zones/{zone}/instances', json={'name':'amd45','machineType':f'zones/{zone}/machineTypes/e2-micro','networkInterfaces':[{'network':f'https://www.googleapis.com/compute/v1/projects/{project}/global/networks/network45','stackType':'IPV4_IPV6'}],'disks':[{'boot':True,'initializeParams':{'diskSizeGb':'30','sourceImage':'projects/ubuntu-os-cloud/global/images/family/ubuntu-2204-lts'}}], 'metadata':{'items':[{'key':'ssh-keys','value':'ubuntu:' + publicKey}]}}).json())

asyncio.run(gcloud())

async def gcloud():
    async with session.patch(f'https://compute.googleapis.com/compute/v1/projects/{project}/global/firewalls/default-allow-ssh', headers={'authorization':f'Bearer {credentials.token}'}, json={'name':'default-allow-ssh','allowed':[{'IPProtocol':'tcp'}]}) as _: pass
    instance = f'https://compute.googleapis.com/compute/v1/projects/{project}/zones/{zone}/instances'
    async with session.get(instance + '/google', headers={'authorization':f'Bearer {credentials.token}'}) as response:
        if response.status == 200:
            async with session.delete(instance + '/google', headers={'authorization':f'Bearer {credentials.token}'}) as _: pass
    while True:
        await asyncio.sleep(60)
        async with session.get(instance + '/google', headers={'authorization':f'Bearer {credentials.token}'}) as response:
            if response.status == 404: break
    async with session.post(instance, headers={'authorization':f'Bearer {credentials.token}'}, json={'name':'google','machineType':f'zones/{zone}/machineTypes/e2-micro','networkInterfaces':[{'accessConfigs':[{'type':'ONE_TO_ONE_NAT','name':'External NAT'}],'network':'global/networks/default','stackType':'IPV4_IPV6'}],'disks':[{'boot':True,'initializeParams':{'diskSizeGb':'30','sourceImage':'projects/ubuntu-os-cloud/global/images/family/ubuntu-2204-lts'}}], 'metadata':{'items':[{'key':'ssh-keys','value':'ubuntu:' + key.export_public_key().decode()}]}}) as _: pass
    await asyncio.sleep(5)
    async with session.get(instance + '/google', headers={'authorization':f'Bearer {credentials.token}'}) as response:
        ip = (await response.json()).get('networkInterfaces')[0].get('accessConfigs')[0].get('natIP')
        await asyncio.sleep(60)
        async with asyncssh.connect(ip, username='ubuntu', known_hosts=None) as ssh: await ssh.run(init)
        return ip
#gcloud auth activate-service-account --key-file=gcloud --project chaowenguo
#gcloud compute networks create google --subnet-mode custom --enable-ula-internal-ipv6
#gcloud compute networks subnets create google --range 10.0.0.0/24 --stack-type IPV4_IPV6 --ipv6-access-type EXTERNAL --network google --region us-central1
#gcloud compute firewall-rules create google-allow-ssh --allow tcp --network google
#gcloud compute firewall-rules create google-allow-ipv6-ssh --allow tcp --network google
#gcloud compute instances create google --image-family=ubuntu-2204-lts --image-project=ubuntu-os-cloud --machine-type=e2-micro --zone=us-central1-a --boot-disk-size=30GB --subnet google --stack-type IPV4_IPV6 --metadata=ssh-keys=ubuntu:
#https://cloud.google.com/vpc/docs/create-modify-vpc-networks#add-subnet-ipv6
#https://cloud.google.com/compute/docs/ip-addresses/configure-ipv6-address#console
#https://cloud.google.com/vpc/docs/create-modify-vpc-networks#create-network-dual

subscription = 'bc64100e-dcaf-4e19-b6ca-46872d453f08'
location = 'westus2' 

async def linux(session, token, subnet):
    async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/machine/providers/Microsoft.Network/publicIPAddresses/linux', params={'api-version':'2023-09-01'}, headers={'authorization':f'Bearer {token}'}, json={'location':location}) as ip:
        if ip.status == 201:
            headers = ip.headers
            while True:
                await asyncio.sleep(builtins.int(headers.get('retry-after')))
                async with session.get(headers.get('azure-asyncOperation'), headers={'authorization':f'Bearer {token}'}) as _:
                    if (await _.json()).get('status') == 'Succeeded': break
        async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/machine/providers/Microsoft.Network/networkInterfaces/linux', params={'api-version':'2023-09-01'}, headers={'authorization':f'Bearer {token}'}, json={'location':location, 'properties':{'ipConfigurations':[{'name':'linux', 'properties':{'publicIPAddress':{'id':(await ip.json()).get('id')}, 'subnet':{'id':subnet}}}]}}) as interface:
            #if interface.status == 201:
            #    while True:
            #        await asyncio.sleep(10)
            #        async with session.get(interface.headers.get('azure-asyncOperation'), headers={'authorization':f'Bearer {token}'}) as _:
            #            if (await _.json()).get('status') == 'Succeeded': break
            async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/machine/providers/Microsoft.Compute/virtualMachines/linux', params={'api-version':'2024-03-01'}, headers={'authorization':f'Bearer {token}'}, json={'location':location, 'properties':{'hardwareProfile':{'vmSize':'Standard_B1s'}, 'osProfile':{'adminUsername':'ubuntu', 'computerName':'linux', 'linuxConfiguration':{'ssh':{'publicKeys':[{'path':'/home/ubuntu/.ssh/authorized_keys', 'keyData':publicKey}]}, 'disablePasswordAuthentication':True}}, 'storageProfile':{'imageReference':{'sku':'server-gen1', 'publisher':'Canonical', 'version':'latest', 'offer':'ubuntu-24_04-lts-daily'}, 'osDisk':{'diskSizeGB':64, 'createOption':'FromImage'}}, 'networkProfile':{'networkInterfaces':[{'id':(await interface.json()).get('id')}]}}}) as machine:
                if machine.status == 201:
                    while True:
                        await asyncio.sleep(builtins.int(machine.headers.get('retry-after')))
                        async with session.get(machine.headers.get('azure-asyncOperation'), headers={'authorization':f'Bearer {token}'}) as _:
                            if (await _.json()).get('status') == 'Succeeded': break
    async with session.post(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/machine/providers/Microsoft.Compute/virtualMachines/linux/runCommand', params={'api-version':'2022-11-01'}, headers={'authorization':f'Bearer {token}'}, json={'commandId':'RunShellScript', 'script':['apt purge -y snapd', 'curl -O https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb', 'sudo apt update', 'apt install -y --no-install-recommends podman uidmap slirp4netns fuse-overlayfs catatonit ./google-chrome-stable_current_amd64.deb libx11-xcb1 x2goserver-xsession fonts-noto-cjk', 'rm google-chrome-stable_current_amd64.deb', 'apt clean']}) as command:
        if command.status == 202:
            while True:
                await asyncio.sleep(10)
                async with session.get(command.headers.get('azure-asyncOperation'), headers={'authorization':f'Bearer {token}'}) as _:
                    if (await _.json()).get('status') == 'Succeeded': break
    await asyncio.sleep(30)
    async with session.get(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/machine/providers/Microsoft.Network/publicIPAddresses/linux', params={'api-version':'2023-09-01'}, headers={'authorization':f'Bearer {token}'}) as response:
        ip = (await response.json()).get('properties').get('ipAddress')
        return ip

#az network vnet create -g machine -l westus2 -n machine --address-prefixes 10.0.0.0/16 2404:f800:8000:122::/63 --subnet-name machine --subnet-prefixes 10.0.0.0/24 2404:f800:8000:122::/64
#az network public-ip create -g machine -n linux4 --sku Standard --version IPv4 --zone 1
#az network public-ip create -g machine -n linux6 --sku Standard --version IPv6 --zone 1
#az network nic create -g machine -n linux --vnet-name machine --subnet machine --public-ip-address linux4
#az network nic ip-config create -g machine -n linux --nic-name linux --private-ip-address-version IPv6 --vnet-name machine --subnet machine --public-ip-address linux6
#az vm create -g linux -n linux --nics linux --image Canonical:ubuntu-24_04-lts-daily:server-gen1:latest --size Standard_B1s --admin-username ubuntu --os-disk-size-gb 64 --ssh-key-values azure.pub
#az vm image list --publisher Canonical --architecture x64 --sku server-gen1 --offer ubuntu-24_04-lts-daily --all --output json#https://docs.microsoft.com/en-us/azure/virtual-machines/linux/tutorial-manage-vm#understand-vm-images
#az vm list-sizes --location westus2 --output json#https://docs.microsoft.com/en-us/azure/virtual-machines/linux/tutorial-manage-vm#find-available-vm-sizes
#https://docs.microsoft.com/en-us/azure/virtual-machines/linux/nsg-quickstart#quickly-open-a-port-for-a-vm
#az vm open-port -g linux -n linux --port 443
#az vm show -d -g linux -n linux --query publicIps -o tsv

async def win(session, token, subnet):
    async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/machine/providers/Microsoft.Network/publicIPAddresses/win', params={'api-version':'2022-07-01'}, headers={'authorization':f'Bearer {token}'}, json={'location':location}) as ip:
        if ip.status == 201:
            headers = ip.headers
            while True:
                await asyncio.sleep(builtins.int(headers.get('retry-after')))
                async with session.get(headers.get('azure-asyncOperation'), headers={'authorization':f'Bearer {token}'}) as _:
                    if (await _.json()).get('status') == 'Succeeded': break
        async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/machine/providers/Microsoft.Network/networkInterfaces/win', params={'api-version':'2023-09-01'}, headers={'authorization':f'Bearer {token}'}, json={'location':location, 'properties':{'ipConfigurations':[{'name':'win', 'properties':{'publicIPAddress':{'id':(await ip.json()).get('id')}, 'subnet':{'id':subnet}}}]}}) as interface:
            #if interface.status == 201:
            #    while True:
            #        await asyncio.sleep(10)
            #        async with session.get(interface.headers.get('azure-asyncOperation'), headers={'authorization':f'Bearer {token}'}) as _:
            #            if (await _.json()).get('status') == 'Succeeded': break
            async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/machine/providers/Microsoft.Compute/virtualMachines/win', params={'api-version':'2024-03-01'}, headers={'authorization':f'Bearer {token}'}, json={'location':location, 'properties':{'hardwareProfile':{'vmSize':'Standard_B1s'}, 'osProfile':{'adminUsername':'ubuntu', 'computerName':'win', 'adminPassword':parser.parse_args().password}, 'storageProfile':{'imageReference':{'sku':'2022-datacenter-azure-edition-core-smalldisk', 'publisher':'MicrosoftWindowsServer', 'version':'latest', 'offer':'WindowsServer'}, 'osDisk':{'diskSizeGB':64, 'createOption':'FromImage'}}, 'networkProfile':{'networkInterfaces':[{'id':(await interface.json()).get('id')}]}}}) as machine:
                if machine.status == 201:
                    while True:
                        await asyncio.sleep(builtins.int(machine.headers.get('retry-after')))
                        async with session.get(machine.headers.get('azure-asyncOperation'), headers={'authorization':f'Bearer {token}'}) as _:
                            if (await _.json()).get('status') == 'Succeeded': break
    async with session.post(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/machine/providers/Microsoft.Compute/virtualMachines/win/runCommand', params={'api-version':'2024-03-01'}, headers={'authorization':f'Bearer {token}'}, json={'commandId':'RunPowerShellScript', 'script':['Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0', 'Start-Service sshd', f'Set-Content -Force -Path C:/ProgramData/ssh/administrators_authorized_keys -Value "{key.export_public_key().decode().replace(builtins.chr(10), "")}"', 'icacls C:/ProgramData/ssh/administrators_authorized_keys /inheritance:r /grant Administrators:`(F`) /grant SYSTEM:`(F`)', '(cat C:/ProgramData/ssh/sshd_config).replace("#PasswordAuthentication yes", "PasswordAuthentication no") | Set-Content -Path C:/ProgramData/ssh/sshd_config', 'Restart-Service sshd', 'Set-Service -Name sshd -StartupType Automatic', 'New-ItemProperty -Path HKLM:/SOFTWARE/OpenSSH -Name DefaultShell -Value C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe -PropertyType String -Force', 'Add-WindowsCapability -Online -Name ServerCore.AppCompatibility~~~~0.0.1.0', 'mkdir c:/Certbot/live']}) as command:
        if command.status == 202:
            while True:
                await asyncio.sleep(10)
                async with session.get(command.headers.get('azure-asyncOperation'), headers={'authorization':f'Bearer {token}'}) as _:
                   if (await _.json()).get('status') == 'Succeeded': break
    await asyncio.sleep(30)
    async with session.get(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/machine/providers/Microsoft.Network/publicIPAddresses/win', params={'api-version':'2023-09-01'}, headers={'authorization':f'Bearer {token}'}) as response:
        ip = (await response.json()).get('properties').get('ipAddress')
        return ip
#az vm create -n win -g machine --image MicrosoftWindowsServer:WindowsServer:2022-datacenter-azure-edition-core-smalldisk:latest --size Standard_B1s --admin-username chaowenguo --admin-password 密码 --os-disk-size-gb 64 --availability-set machine --vnet-name machine --subnet machine --nsg ''
#az vm open-port -g win -n win --port 22
                    
async def main():
    redis = aredis.StrictRedis(host='immune-eagle-44408.upstash.io', password=parser.parse_args().redis, ssl=True)
    async with aiohttp.ClientSession() as session:
        async with session.post(f'https://login.microsoftonline.com/{(await redis.hget(subscription, "tenant")).decode()}/oauth2/token', data={'grant_type':'client_credentials', 'client_id':(await redis.hget(subscription, 'id')).decode(), 'client_secret':(await redis.hget(subscription, 'secret')).decode(), 'resource':'https://management.azure.com/'}) as response:
            redis.connection_pool.disconnect()
            token = (await response.json()).get('access_token')
            group = f'https://management.azure.com/subscriptions/{subscription}/resourcegroups/machine?api-version=2021-04-01'
            async with session.head(group, headers={'authorization':f'Bearer {token}'}) as response:
                if response.status == 204:
                    async with session.delete(group, headers={'authorization':f'Bearer {token}'}) as response:
                        if response.status == 202:
                            while True:
                                await asyncio.sleep(builtins.int(response.headers.get('retry-after')))
                                async with session.get(response.headers.get('location'), headers={'authorization':f'Bearer {token}'}) as _:
                                    if _.status == 200: break
            async with session.put(group, headers={'authorization':f'Bearer {token}'}, json={'location':location}) as _: pass
            async with session.put(f'https://management.azure.com/subscriptions/{subscription}/resourceGroups/machine/providers/Microsoft.Network/virtualNetworks/machine', params={'api-version':'2023-09-01'}, headers={'authorization':f'Bearer {token}'}, json={'location':location, 'properties':{'addressSpace':{'addressPrefixes':['10.0.0.0/16', '2404:f800:8000:122::/63']}, 'subnets':[{'name':'machine', 'properties':{'addressPrefixes':['10.0.0.0/24', '2404:f800:8000:122::/64']}}]}}) as network:
                if network.status == 201:
                    while True:
                        await asyncio.sleep(builtins.int(network.headers.get('retry-after')))
                        async with session.get(network.headers.get('azure-asyncOperation'), headers={'authorization':f'Bearer {token}'}) as _:
                            if (await _.json()).get('status') == 'Succeeded': break
                subnet = (await network.json()).get('properties').get('subnets')[0].get('id')
                print(await asyncio.gather(linux(session, token, subnet), win(session, token, subnet)))

asyncio.run(main())
#https://51.ruyo.net/14138.html#13 oci 
#/usr/bin/chromium-browser --no-sandbox --no-first-run --remote-debugging-port=9222#https://localhost:9222/json/version to get websocket
#if `az group exists -n machine`
#then
#    az group delete -n machine -y
#fi
#az group create -n machine -l westus2