import {promises as fs} from 'fs'
import sshpk from 'sshpk'
import path from 'path'
import os from 'os'
import ioredis from 'ioredis'
import * as commander from '/usr/share/nodejs/commander/esm.mjs'

commander.program.requiredOption('--redis <>').requiredOption('--password <>')
commander.program.parse()
const subscription = '493d8adb-4bc5-4e29-9341-e44e8c71df19'
const publicKey = sshpk.parsePrivateKey(await fs.readFile(path.join(os.homedir(), '.ssh', 'id_ed25519'))).toPublic().toString().split(' ', 2).join(' ')
const location = 'eastus2' 

async function linux(token, subnet)
{
    const ip = await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourceGroups/machine/providers/Microsoft.Network/publicIPAddresses/linux?api-version=2023-09-01`, {method:'put', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({location/*, zones:['1']*/})})
    if (globalThis.Object.is(ip.status, 201))
    {
        const headers = ip.headers
        while (true)
        {
            await new globalThis.Promise(_ => globalThis.setTimeout(_, headers.get('retry-after') * 1000))
            if (globalThis.Object.is((await globalThis.fetch(headers.get('azure-asyncOperation'), {headers:{authorization:`Bearer ${token}`}}).then(_ => _.json())).status, 'Succeeded')) break
        }
    }
    const networkInterface = await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourceGroups/machine/providers/Microsoft.Network/networkInterfaces/linux?api-version=2023-09-01`, {method:'put', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({location, properties:{ipConfigurations:[{name:'linux', properties:{publicIPAddress:{id:(await ip.json()).id}, subnet:{id:subnet}}}]}})})
    /*if (globalThis.Object.is(networkInterface.status, 201))
    {
        while (true)
        {
            await new globalThis.Promise(_ => globalThis.setTimeout(_, 10 * 1000))
            if (globalThis.Object.is((await globalThis.fetch(networkInterface.headers.get('azure-asyncOperation'), {headers:{authorization:`Bearer ${token}`}}).then(_ => _.json())).status, 'Succeeded')) break
        }
    }*/
    const machine = await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourceGroups/machine/providers/Microsoft.Compute/virtualMachines/linux?api-version=2024-03-01`, {method:'put', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({location, properties:{hardwareProfile:{vmSize:'Standard_B1s'}, osProfile:{adminUsername:'ubuntu', computerName:'linux', linuxConfiguration:{ssh:{publicKeys:[{path:'/home/ubuntu/.ssh/authorized_keys', keyData:publicKey}]}, disablePasswordAuthentication:true}}, storageProfile:{imageReference:{sku:'server-gen1', publisher:'Canonical', version:'latest', offer:'ubuntu-24_04-lts-daily'}, osDisk:{diskSizeGB:64, createOption:'FromImage'}}, networkProfile:{networkInterfaces:[{id:(await networkInterface.json()).id}]}}/*, zones:['1']*/})})
    if (globalThis.Object.is(machine.status, 201))
    {
        while (true)
        {
            await new globalThis.Promise(_ => globalThis.setTimeout(_, machine.headers.get('retry-after') * 1000))
            if (globalThis.Object.is((await globalThis.fetch(machine.headers.get('azure-asyncOperation'), {headers:{authorization:`Bearer ${token}`}}).then(_ => _.json())).status, 'Succeeded')) break
        }
    }
    const command = await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourceGroups/machine/providers/Microsoft.Compute/virtualMachines/linux/runCommand?api-version=2024-03-01`, {method:'post', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({commandId:'RunShellScript', script:['apt purge -y snapd', 'curl -O https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb', 'sudo apt update', 'apt install -y --no-install-recommends podman uidmap slirp4netns fuse-overlayfs catatonit ./google-chrome-stable_current_amd64.deb libx11-xcb1 x2goserver-xsession fonts-noto-cjk', 'rm google-chrome-stable_current_amd64.deb', 'apt clean']})})
    if (globalThis.Object.is(command.status, 202))
    {
        while (true)
        {
            await new globalThis.Promise(_ => globalThis.setTimeout(_, 10 * 1000))
            if (globalThis.Object.is((await globalThis.fetch(command.headers.get('azure-asyncOperation'), {headers:{authorization:`Bearer ${token}`}}).then(_ => _.json())).status, 'Succeeded')) break
        }
    }
    await new globalThis.Promise(_ => globalThis.setTimeout(_, 30 * 1000))
    return await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourceGroups/machine/providers/Microsoft.Network/publicIPAddresses/linux?api-version=2023-09-01`, {headers:{authorization:`Bearer ${token}`}}).then(_ => _.json()).then(_ => _.properties.ipAddress)
}
                                                                                                                       
async function win(token, subnet)
{
    const ip = await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourceGroups/machine/providers/Microsoft.Network/publicIPAddresses/win?api-version=2023-09-01`, {method:'put', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({location/*, zones:['2']*/})})
    if (globalThis.Object.is(ip.status, 201))
    {
        const headers = ip.headers
        while (true)
        {
            await new globalThis.Promise(_ => globalThis.setTimeout(_, headers.get('retry-after') * 1000))
            if (globalThis.Object.is((await globalThis.fetch(headers.get('azure-asyncOperation'), {headers:{authorization:`Bearer ${token}`}}).then(_ => _.json())).status, 'Succeeded')) break
        }
    }
    const networkInterface = await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourceGroups/machine/providers/Microsoft.Network/networkInterfaces/win?api-version=2023-09-01`, {method:'put', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({location, properties:{ipConfigurations:[{name:'win', properties:{publicIPAddress:{id:(await ip.json()).id}, subnet:{id:subnet}}}]}})})
    /*if (globalThis.Object.is(networkInterface.status, 201))
    {
        while (true)
        {
            await new globalThis.Promise(_ => globalThis.setTimeout(_, 10 * 1000))
            if (globalThis.Object.is((await globalThis.fetch(networkInterface.headers.get('azure-asyncOperation'), {headers:{authorization:`Bearer ${token}`}}).then(_ => _.json())).status, 'Succeeded')) break
        }
    }*/
    const machine = await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourceGroups/machine/providers/Microsoft.Compute/virtualMachines/win?api-version=2024-03-01`, {method:'put', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({location, properties:{hardwareProfile:{vmSize:'Standard_B1s'}, osProfile:{adminUsername:'ubuntu', computerName:'win', 'adminPassword':commander.program.opts().password}, storageProfile:{imageReference:{sku:'2022-datacenter-azure-edition-core-smalldisk', publisher:'MicrosoftWindowsServer', version:'latest', offer:'WindowsServer'}, osDisk:{diskSizeGB:64, createOption:'FromImage'}}, networkProfile:{networkInterfaces:[{id:(await networkInterface.json()).id}]}}/*, zones:['2']*/})})
    if (globalThis.Object.is(machine.status, 201))
    {
        while (true)
        {
            await new globalThis.Promise(_ => globalThis.setTimeout(_, machine.headers.get('retry-after') * 1000))
            if (globalThis.Object.is((await globalThis.fetch(machine.headers.get('azure-asyncOperation'), {headers:{authorization:`Bearer ${token}`}}).then(_ => _.json())).status, 'Succeeded')) break
        }
    }
    const command = await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourceGroups/machine/providers/Microsoft.Compute/virtualMachines/win/runCommand?api-version=2024-03-01`, {method:'post', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({commandId:'RunPowerShellScript', script:['Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0', 'Start-Service sshd', `Set-Content -Force -Path C:/ProgramData/ssh/administrators_authorized_keys -Value "${publicKey}"`, 'icacls C:/ProgramData/ssh/administrators_authorized_keys /inheritance:r /grant Administrators:`(F`) /grant SYSTEM:`(F`)', '(cat C:/ProgramData/ssh/sshd_config).replace("#PasswordAuthentication yes", "PasswordAuthentication no") | Set-Content -Path C:/ProgramData/ssh/sshd_config', 'Restart-Service sshd', 'Set-Service -Name sshd -StartupType Automatic', 'New-ItemProperty -Path HKLM:/SOFTWARE/OpenSSH -Name DefaultShell -Value C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe -PropertyType String -Force', 'Add-WindowsCapability -Online -Name ServerCore.AppCompatibility~~~~0.0.1.0', 'mkdir c:/Certbot/live']})})
    if (globalThis.Object.is(command.status, 202))
    {
        while (true)
        {
            await new globalThis.Promise(_ => globalThis.setTimeout(_, 10 * 1000))
            if (globalThis.Object.is((await globalThis.fetch(command.headers.get('azure-asyncOperation'), {headers:{authorization:`Bearer ${token}`}}).then(_ => _.json())).status, 'Succeeded')) break
        }
    }
    await new globalThis.Promise(_ => globalThis.setTimeout(_, 30 * 1000))
    return await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourceGroups/machine/providers/Microsoft.Network/publicIPAddresses/win?api-version=2023-09-01`, {headers:{authorization:`Bearer ${token}`}}).then(_ => _.json()).then(_ => _.properties.ipAddress)
}

const redis = new ioredis.Redis({host:'immune-eagle-44408.upstash.io', password:commander.program.opts().redis, tls:{}})
const token = await globalThis.fetch(`https://login.microsoftonline.com/${await redis.hget(subscription, 'tenant')}/oauth2/token`, {method:'post', body:new globalThis.URLSearchParams({grant_type:'client_credentials', client_id:await redis.hget(subscription, 'id'), client_secret:await redis.hget(subscription, 'secret'), resource:'https://management.azure.com/'})}).then(_ => _.json()).then(_ => _.access_token)
await redis.quit()
const group = `https://management.azure.com/subscriptions/${subscription}/resourcegroups/machine?api-version=2021-04-01` 
if (globalThis.Object.is((await globalThis.fetch(group, {method:'head', headers:{authorization:`Bearer ${token}`}})).status, 204))
{
    const response = await globalThis.fetch(group, {method:'delete',  headers:{authorization:`Bearer ${token}`}})
    if (globalThis.Object.is(response.status, 202))
    {
        while (true)
        {
            await new globalThis.Promise(_ => globalThis.setTimeout(_, response.headers.get('retry-after') * 1000))
            if (globalThis.Object.is(await globalThis.fetch(response.headers.get('location'), {headers:{authorization:`Bearer ${token}`}}).then(_ => _.status), 200)) break
        }
    }
}
await globalThis.fetch(group, {method:'put', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({location})})
const network = await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourceGroups/machine/providers/Microsoft.Network/virtualNetworks/machine?api-version=2023-09-01`, {method:'put', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({location, properties:{addressSpace:{addressPrefixes:['10.0.0.0/16', '2404:f800:8000:122::/63']}, subnets:[{name:'machine', properties:{addressPrefixes:['10.0.0.0/24', '2404:f800:8000:122::/64']}}]}})})
if (globalThis.Object.is(network.status, 201))
{
    while (true)
    {
        await new globalThis.Promise(_ => globalThis.setTimeout(_, network.headers.get('retry-after') * 1000))
        if (globalThis.Object.is((await globalThis.fetch(network.headers.get('azure-asyncOperation'), {headers:{authorization:`Bearer ${token}`}}).then(_ => _.json())).status, 'Succeeded')) break
    }
}
const subnet = (await network.json()).properties.subnets[0].id
console.log(await globalThis.Promise.all([linux(token, subnet), win(token, subnet)]))
//await globalThis.fetch('https://api.github.com/repos/chaowenGUO/key/contents/0', {method:'put', headers:{'authorization':`token ${process.argv.at(2)}`}, body:globalThis.JSON.stringify({message:'message', content:globalThis.btoa(globalThis.JSON.stringify(await linux(token, subnet)))})})