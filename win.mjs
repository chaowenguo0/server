import process from 'process'
import {promises as fs} from 'fs'
import sshpk from 'sshpk'
import path from 'path'
import os from 'os'

const subscription = '326ccd13-f7e0-4fbf-be40-22e42ef93ad5'

const token = await globalThis.fetch('https://login.microsoftonline.com/deb7ba76-72fc-4c07-833f-1628b5e92168/oauth2/token', {method:'post', body:new globalThis.URLSearchParams({grant_type:'client_credentials', client_id:'a48cc3b9-fa35-4e5a-8aea-2cef8eb4eb74', client_secret:'w2B8Q~gFN6qV66LILUFLuccS1EoQ2.jFPYWGqctb', resource:'https://management.azure.com/'})}).then(_ => _.json()).then(_ => _.access_token)
const machine = await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourceGroups/machine/providers/Microsoft.Compute/virtualMachines/win?api-version=2021-07-01`, {method:'put', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({location:'westus2', properties:{hardwareProfile:{vmSize:'Standard_B1s'}, osProfile:{adminUsername:'ubuntu', computerName:'win', 'adminPassword':process.argv.at(2)}, storageProfile:{imageReference:{sku:'2022-datacenter-azure-edition-core-smalldisk', publisher:'MicrosoftWindowsServer', version:'latest', offer:'WindowsServer'}, osDisk:{diskSizeGB:64, createOption:'FromImage'}}, networkProfile:{networkInterfaces:[{id:`/subscriptions/${subscription}/resourceGroups/machine/providers/Microsoft.Network/networkInterfaces/win`}]}}, zones:['2']})})
if (globalThis.Object.is(machine.status, 201))
{
    while (true)
    {
        await new globalThis.Promise(_ => globalThis.setTimeout(_, machine.headers.get('retry-after') * 1000))
        if (globalThis.Object.is((await globalThis.fetch(machine.headers.get('azure-asyncOperation'), {headers:{authorization:`Bearer ${token}`}}).then(_ => _.json())).status, 'Succeeded')) break
    }
}
const publicKey = sshpk.parsePrivateKey(await fs.readFile(path.join(os.homedir(), '.ssh', 'id_ed25519'))).toPublic().toString().split(' ', 2).join(' ')
const command = await globalThis.fetch(`https://management.azure.com/subscriptions/${subscription}/resourceGroups/machine/providers/Microsoft.Compute/virtualMachines/win/runCommand?api-version=2022-11-01`, {method:'post', headers:{authorization:`Bearer ${token}`, 'content-type':'application/json'}, body:globalThis.JSON.stringify({commandId:'RunPowerShellScript', script:['Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0', 'Start-Service sshd', `Set-Content -Force -Path C:/ProgramData/ssh/administrators_authorized_keys -Value "${publicKey}"`, 'icacls C:/ProgramData/ssh/administrators_authorized_keys /inheritance:r /grant Administrators:`(F`) /grant SYSTEM:`(F`)', '(cat C:/ProgramData/ssh/sshd_config).replace("#PasswordAuthentication yes", "PasswordAuthentication no") | Set-Content -Path C:/ProgramData/ssh/sshd_config', 'Restart-Service sshd', 'Set-Service -Name sshd -StartupType Automatic', 'New-ItemProperty -Path HKLM:/SOFTWARE/OpenSSH -Name DefaultShell -Value C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe -PropertyType String -Force', 'Add-WindowsCapability -Online -Name ServerCore.AppCompatibility~~~~0.0.1.0', 'mkdir c:/Certbot/live']})})
if (globalThis.Object.is(command.status, 202))
{
    while (true)
    {
        await new globalThis.Promise(_ => globalThis.setTimeout(_, 10 * 1000))
        if (globalThis.Object.is((await globalThis.fetch(command.headers.get('azure-asyncOperation'), {headers:{authorization:`Bearer ${token}`}}).then(_ => _.json())).status, 'Succeeded')) break
    }
}
