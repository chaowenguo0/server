import asyncio, pathlib, builtins, datetime

async def loottv():
    while True:
        await asyncio.create_subprocess_exec('/usr/local/bin/qsub', '-l', 'walltime=24:00:00', builtins.str(pathlib.Path().joinpath('loottv.sh')))
        await asyncio.sleep(datetime.timedelta(hours=24, minutes=5).total_seconds())

async def main():
    await asyncio.gather(loottv())

loop = asyncio.get_event_loop()
loop.run_until_complete(main())
loop.close()