public class Server
{
    static final java.lang.String subscription = "f2b1ad0f-72dc-4328-ab3c-4c84d84697ce";
    static final java.lang.String location = "westus";
    static final java.net.http.HttpClient client = java.net.http.HttpClient.newHttpClient();
    static final com.fasterxml.jackson.databind.ObjectMapper objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
    static java.lang.String publicKey;
    static java.lang.String password;
    static
    {
	try
	{
            publicKey = "ssh-ed25519 " + java.util.Base64.getEncoder().encodeToString(org.bouncycastle.crypto.util.OpenSSHPublicKeyUtil.encodePublicKey(((org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters)org.bouncycastle.crypto.util.OpenSSHPrivateKeyUtil.parsePrivateKeyBlob(java.util.Base64.getDecoder().decode(java.nio.file.Files.readAllLines(java.nio.file.Path.of(java.lang.System.getProperty("user.home"), ".ssh/id_ed25519")).stream().filter($ -> !$.contains("-")).collect(java.util.stream.Collectors.joining())))).generatePublicKey()));
	}
	catch (final java.lang.Exception e){}
    }
    private Server(final java.lang.String password)
    {
	this.password = password;
    } 
    
    private java.lang.String linux(final java.lang.String token, final java.lang.String subnet) throws java.lang.Exception
    {
	final var ip = this.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(STR."https://management.azure.com/subscriptions/\{this.subscription}/resourceGroups/machine/providers/Microsoft.Network/publicIPAddresses/linux?api-version=2023-09-01")).headers("authorization", STR."Bearer \{token}", "content-type", "application/json").PUT(java.net.http.HttpRequest.BodyPublishers.ofString(this.objectMapper.writeValueAsString(java.util.Map.of("location", this.location)))).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).join();
        if (ip.statusCode() == 201)
        {
            final var headers = ip.headers();
            while (true)
            {
                java.util.concurrent.TimeUnit.SECONDS.sleep(headers.firstValueAsLong("retry-after").getAsLong());
                if (this.objectMapper.readTree(this.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(headers.firstValue("azure-asyncOperation").get())).header("authorization", STR."Bearer \{token}").GET().build(), java.net.http.HttpResponse.BodyHandlers.ofString()).thenApply(java.net.http.HttpResponse::body).join()).get("status").asText().equals("Succeeded")) break;
            }
        }
        final var networkInterface = this.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(STR."https://management.azure.com/subscriptions/\{this.subscription}/resourceGroups/machine/providers/Microsoft.Network/networkInterfaces/linux?api-version=2023-09-01")).headers("authorization", STR."Bearer \{token}", "content-type", "application/json").PUT(java.net.http.HttpRequest.BodyPublishers.ofString(this.objectMapper.writeValueAsString(java.util.Map.of("location", this.location, "properties", java.util.Map.of("ipConfigurations", java.util.List.of(java.util.Map.of("name", "linux", "properties", java.util.Map.of("publicIPAddress", java.util.Map.of("id", this.objectMapper.readTree(ip.body()).get("id").asText()), "subnet", java.util.Map.of("id", subnet))))))))).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).join();
	final var machine = this.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(STR."https://management.azure.com/subscriptions/\{this.subscription}/resourceGroups/machine/providers/Microsoft.Compute/virtualMachines/linux?api-version=2024-03-01")).headers("authorization", STR."Bearer \{token}", "content-type", "application/json").PUT(java.net.http.HttpRequest.BodyPublishers.ofString(this.objectMapper.writeValueAsString(java.util.Map.of("location", this.location, "properties", java.util.Map.of("hardwareProfile", java.util.Map.of("vmSize", "Standard_B1s"), "osProfile", java.util.Map.of("adminUsername", "ubuntu", "computerName", "linux", "linuxConfiguration", java.util.Map.of("ssh", java.util.Map.of("publicKeys", java.util.List.of(java.util.Map.of("path", "/home/ubuntu/.ssh/authorized_keys", "keyData", this.publicKey))), "disablePasswordAuthentication", true)), "storageProfile", java.util.Map.of("imageReference", java.util.Map.of("sku", "server-gen1", "publisher", "Canonical", "version", "latest", "offer", "ubuntu-24_04-lts-daily"), "osDisk", java.util.Map.of("diskSizeGB", 64, "createOption", "FromImage")), "networkProfile", java.util.Map.of("networkInterfaces", java.util.List.of(java.util.Map.of("id", this.objectMapper.readTree(networkInterface.body()).get("id").asText())))))))).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).join();
        if (machine.statusCode() == 201)
        {
            while (true)
            {
                java.util.concurrent.TimeUnit.SECONDS.sleep(machine.headers().firstValueAsLong("retry-after").getAsLong());
                if (this.objectMapper.readTree(this.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(machine.headers().firstValue("azure-asyncOperation").get())).header("authorization", STR."Bearer \{token}").GET().build(), java.net.http.HttpResponse.BodyHandlers.ofString()).thenApply(java.net.http.HttpResponse::body).join()).get("status").asText().equals("Succeeded")) break;
            }
        }
        final var command = this.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(STR."https://management.azure.com/subscriptions/\{subscription}/resourceGroups/machine/providers/Microsoft.Compute/virtualMachines/linux/runCommand?api-version=2024-03-01")).headers("authorization", STR."Bearer \{token}", "content-type", "application/json").POST(java.net.http.HttpRequest.BodyPublishers.ofString(this.objectMapper.writeValueAsString(java.util.Map.of("commandId", "RunShellScript", "script", java.util.List.of("apt purge -y snapd", "curl -O https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb", "sudo apt update", "apt install -y --no-install-recommends podman uidmap slirp4netns fuse-overlayfs catatonit ./google-chrome-stable_current_amd64.deb libx11-xcb1 x2goserver-xsession fonts-noto-cjk", "rm google-chrome-stable_current_amd64.deb", "apt clean"))))).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).join();		
        if (command.statusCode() == 202)
        {
            while (true)
            {
                java.util.concurrent.TimeUnit.SECONDS.sleep(10);
                if (this.objectMapper.readTree(this.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(command.headers().firstValue("azure-asyncOperation").get())).header("authorization", STR."Bearer \{token}").GET().build(), java.net.http.HttpResponse.BodyHandlers.ofString()).thenApply(java.net.http.HttpResponse::body).join()).get("status").asText().equals("Succeeded")) break;
            }
        }
        java.util.concurrent.TimeUnit.SECONDS.sleep(30);
        return this.objectMapper.readTree(this.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(STR."https://management.azure.com/subscriptions/\{this.subscription}/resourceGroups/machine/providers/Microsoft.Network/publicIPAddresses/linux?api-version=2023-09-01")).headers("authorization", STR."Bearer \{token}").GET().build(), java.net.http.HttpResponse.BodyHandlers.ofString()).thenApply(java.net.http.HttpResponse::body).join()).get("properties").get("ipAddress").asText();
    }
     
    private java.lang.String win(final java.lang.String token, final java.lang.String subnet) throws java.lang.Exception
    {
	final var ip = this.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(STR."https://management.azure.com/subscriptions/\{subscription}/resourceGroups/machine/providers/Microsoft.Network/publicIPAddresses/win?api-version=2023-09-01")).headers("authorization", STR."Bearer \{token}", "content-type", "application/json").PUT(java.net.http.HttpRequest.BodyPublishers.ofString(this.objectMapper.writeValueAsString(java.util.Map.of("location", this.location)))).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).join();
        if (ip.statusCode() == 201)
        {
            final var headers = ip.headers();
            while (true)
            {
                java.util.concurrent.TimeUnit.SECONDS.sleep(headers.firstValueAsLong("retry-after").getAsLong());
                if (this.objectMapper.readTree(this.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(headers.firstValue("azure-asyncOperation").get())).header("authorization", STR."Bearer \{token}").GET().build(), java.net.http.HttpResponse.BodyHandlers.ofString()).thenApply(java.net.http.HttpResponse::body).join()).get("status").asText().equals("Succeeded")) break;
            }
        }
        final var networkInterface = this.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(STR."https://management.azure.com/subscriptions/\{subscription}/resourceGroups/machine/providers/Microsoft.Network/networkInterfaces/win?api-version=2023-09-01")).headers("authorization", STR."Bearer \{token}", "content-type", "application/json").PUT(java.net.http.HttpRequest.BodyPublishers.ofString(this.objectMapper.writeValueAsString(java.util.Map.of("location", this.location, "properties", java.util.Map.of("ipConfigurations", java.util.List.of(java.util.Map.of("name", "win", "properties", java.util.Map.of("publicIPAddress", java.util.Map.of("id", this.objectMapper.readTree(ip.body()).get("id").asText()), "subnet", java.util.Map.of("id", subnet))))))))).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).join();      
	final var machine = this.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(STR."https://management.azure.com/subscriptions/\{subscription}/resourceGroups/machine/providers/Microsoft.Compute/virtualMachines/win?api-version=2024-03-01")).headers("authorization", STR."Bearer \{token}", "content-type", "application/json").PUT(java.net.http.HttpRequest.BodyPublishers.ofString(this.objectMapper.writeValueAsString(java.util.Map.of("location", this.location, "properties", java.util.Map.of("hardwareProfile", java.util.Map.of("vmSize", "Standard_B1s"), "osProfile", java.util.Map.of("adminUsername", "ubuntu", "computerName", "win", "adminPassword", this.password), "storageProfile", java.util.Map.of("imageReference", java.util.Map.of("sku", "2022-datacenter-azure-edition-core-smalldisk", "publisher", "MicrosoftWindowsServer", "version", "latest", "offer", "WindowsServer"), "osDisk", java.util.Map.of("diskSizeGB", 64, "createOption", "FromImage")), "networkProfile", java.util.Map.of("networkInterfaces", java.util.List.of(java.util.Map.of("id", this.objectMapper.readTree(networkInterface.body()).get("id").asText())))))))).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).join();
        if (machine.statusCode() == 201)
        {
            while (true)
            {
                java.util.concurrent.TimeUnit.SECONDS.sleep(machine.headers().firstValueAsLong("retry-after").getAsLong());
                if (this.objectMapper.readTree(this.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(machine.headers().firstValue("azure-asyncOperation").get())).header("authorization", STR."Bearer \{token}").GET().build(), java.net.http.HttpResponse.BodyHandlers.ofString()).thenApply(java.net.http.HttpResponse::body).join()).get("status").asText().equals("Succeeded")) break;
            }
        }
        final var command = this.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(STR."https://management.azure.com/subscriptions/\{subscription}/resourceGroups/machine/providers/Microsoft.Compute/virtualMachines/win/runCommand?api-version=2024-03-01")).headers("authorization", STR."Bearer \{token}", "content-type", "application/json").POST(java.net.http.HttpRequest.BodyPublishers.ofString(this.objectMapper.writeValueAsString(java.util.Map.of("commandId", "RunPowerShellScript", "script", java.util.List.of("Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0", "Start-Service sshd", STR."Set-Content -Force -Path C:/ProgramData/ssh/administrators_authorized_keys -Value \"\{this.publicKey}\"", "icacls C:/ProgramData/ssh/administrators_authorized_keys /inheritance:r /grant Administrators:`(F`) /grant SYSTEM:`(F`)", "(cat C:/ProgramData/ssh/sshd_config).replace(\"#PasswordAuthentication yes\", \"PasswordAuthentication no\") | Set-Content -Path C:/ProgramData/ssh/sshd_config", "Restart-Service sshd", "Set-Service -Name sshd -StartupType Automatic", "New-ItemProperty -Path HKLM:/SOFTWARE/OpenSSH -Name DefaultShell -Value C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe -PropertyType String -Force", "Add-WindowsCapability -Online -Name ServerCore.AppCompatibility~~~~0.0.1.0", "mkdir c:/Certbot/live"))))).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).join();		
	if (command.statusCode() == 202)
        {
            while (true)
            {
                java.util.concurrent.TimeUnit.SECONDS.sleep(10);
                if (this.objectMapper.readTree(this.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(command.headers().firstValue("azure-asyncOperation").get())).header("authorization", STR."Bearer \{token}").GET().build(), java.net.http.HttpResponse.BodyHandlers.ofString()).thenApply(java.net.http.HttpResponse::body).join()).get("status").asText().equals("Succeeded")) break;
            }
        }
        java.util.concurrent.TimeUnit.SECONDS.sleep(30);
        return this.objectMapper.readTree(this.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(STR."https://management.azure.com/subscriptions/\{subscription}/resourceGroups/machine/providers/Microsoft.Network/publicIPAddresses/win?api-version=2023-09-01")).headers("authorization", STR."Bearer \{token}").GET().build(), java.net.http.HttpResponse.BodyHandlers.ofString()).thenApply(java.net.http.HttpResponse::body).join()).get("properties").get("ipAddress").asText();
    }

    public static void main(final java.lang.String[] args) throws java.lang.Exception
    {
        final var options = new org.apache.commons.cli.Options();
        options.addOption(org.apache.commons.cli.Option.builder("redis").hasArg().required().build());
        options.addOption(org.apache.commons.cli.Option.builder("password").hasArg().required().build());
        final var parser = new org.apache.commons.cli.DefaultParser();
        final var commandLine = parser.parse(options, args);
        final var jedis = new redis.clients.jedis.JedisPooled(new redis.clients.jedis.HostAndPort("immune-eagle-44408.upstash.io", 6379), redis.clients.jedis.DefaultJedisClientConfig.builder().ssl(true).password(commandLine.getOptionValue("redis")).build());
	final var server = new Server(commandLine.getOptionValue("password")); 
	final var token = server.objectMapper.readTree(server.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(STR."https://login.microsoftonline.com/\{jedis.hget(server.subscription, "tenant")}/oauth2/token")).POST(java.net.http.HttpRequest.BodyPublishers.ofString(java.util.Map.of("grant_type", "client_credentials", "client_id", jedis.hget(server.subscription, "id"), "client_secret", jedis.hget(server.subscription, "secret"), "resource", "https://management.azure.com/").entrySet().stream().map(entry -> java.lang.String.join("=", java.net.URLEncoder.encode(entry.getKey(), java.nio.charset.StandardCharsets.UTF_8), java.net.URLEncoder.encode(entry.getValue(), java.nio.charset.StandardCharsets.UTF_8))).collect(java.util.stream.Collectors.joining("&")))).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).thenApply(java.net.http.HttpResponse::body).join()).get("access_token").asText();
        jedis.close();
        final var group = STR."https://management.azure.com/subscriptions/\{server.subscription}/resourcegroups/machine?api-version=2021-04-01";
        if (server.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(group)).header("authorization", STR."Bearer \{token}").HEAD().build(), java.net.http.HttpResponse.BodyHandlers.discarding()).thenApply(java.net.http.HttpResponse::statusCode).join() == 204)
	{
	    final var response = server.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(group)).header("authorization", STR."Bearer \{token}").DELETE().build(), java.net.http.HttpResponse.BodyHandlers.discarding()).join();
            if (response.statusCode() == 202)
            {
                while (true)
                {
                    java.util.concurrent.TimeUnit.SECONDS.sleep(response.headers().firstValueAsLong("retry-after").getAsLong());
                    if (server.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(response.headers().firstValue("location").get())).header("authorization", STR."Bearer \{token}").GET().build(), java.net.http.HttpResponse.BodyHandlers.discarding()).thenApply(java.net.http.HttpResponse::statusCode).join() == 200) break;
                }
            }
	}
        server.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(group)).headers("authorization", STR."Bearer \{token}", "content-type", "application/json").PUT(java.net.http.HttpRequest.BodyPublishers.ofString(Server.objectMapper.writeValueAsString(java.util.Map.of("location", Server.location)))).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).join();
	final var network = Server.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(STR."https://management.azure.com/subscriptions/\{server.subscription}/resourceGroups/machine/providers/Microsoft.Network/virtualNetworks/machine?api-version=2023-09-01")).headers("authorization", STR."Bearer \{token}", "content-type", "application/json").PUT(java.net.http.HttpRequest.BodyPublishers.ofString(server.objectMapper.writeValueAsString(java.util.Map.of("location", server.location, "properties", java.util.Map.of("addressSpace", java.util.Map.of("addressPrefixes", java.util.List.of("10.0.0.0/16", "2404:f800:8000:122::/63")), "subnets", java.util.List.of(java.util.Map.of("name", "machine", "properties", java.util.Map.of("addressPrefixes", java.util.List.of("10.0.0.0/24", "2404:f800:8000:122::/64"))))))))).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).join();
        if (network.statusCode() == 201)
        {
            while (true)
            {
                java.util.concurrent.TimeUnit.SECONDS.sleep(network.headers().firstValueAsLong("retry-after").getAsLong());
                if (server.objectMapper.readTree(server.client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create(network.headers().firstValue("azure-asyncOperation").get())).header("authorization", STR."Bearer \{token}").GET().build(), java.net.http.HttpResponse.BodyHandlers.ofString()).thenApply(java.net.http.HttpResponse::body).join()).get("status").asText().equals("Succeeded")) break;
            }
        }
        final var subnet = server.objectMapper.readTree(network.body()).get("properties").get("subnets").get(0).get("id").asText();
        final var executor = java.util.concurrent.Executors.newVirtualThreadPerTaskExecutor();
	java.lang.System.out.println(executor.invokeAll(java.util.List.of(() -> server.linux(token, subnet), () -> server.win(token, subnet))).stream().map($ -> {try {return $.get();}catch(final java.lang.Exception e){return null;}}).collect(java.util.stream.Collectors.toList()));
    }
}
//apt install -y --no-install-recommends libjackson2-databind-java libbcpg-java libcommons-cli-java libcommons-pool2-java
//curl --output-dir /usr/share/java https://repo1.maven.org/maven2/redis/clients/jedis/5.1.3/jedis-5.1.3.jar
//java --enable-preview --source 21 -cp /usr/share/java/*:. Server.java